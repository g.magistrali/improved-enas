# Improved-ENAS
An improvement upon a tensorflow implementation (https://github.com/MINGUKKANG/ENAS-Tensorflow) of Efficient Neural Architecture Search (Pham et al, 2018), which focuses on adding a system to modify the reward used during the LSTM controller training phase.

It works by comparing the accuracy of the current network with the average accuracy of the last 10 networks. If it is lower than 70% of the average, the reward is reduced to 0.01, otherwise the reward becomes the accuracy multiplied by the accuracy divided by the average. This means that networks underperforming compared to the trend will be penalized, while ones that are closer either get slightly penalized if lower than the average or get a slight improvement if over.

## Usage

### Setup

You should modify the following code in main_controller_child_trainer.py to suit your specific situation:

```python
DEFINE_string("output_dir", "."+os.path.sep+"output" , "")
DEFINE_string("child_log_filename","child_log.csv","")
DEFINE_string("controller_log_filename","controller_log.csv","")
DEFINE_string("architecture_info_filename","arc_info.json","")
DEFINE_string("best_arcs_filename","best_arcs.csv","")
DEFINE_string("train_data_dir", "."+os.path.sep+"data"+os.path.sep+"train", "")
DEFINE_string("val_data_dir", "."+os.path.sep+"data"+os.path.sep+"valid", "")
DEFINE_string("test_data_dir", "."+os.path.sep+"data"+os.path.sep+"test", "")
DEFINE_integer("channel",3, "MNIST/fashion_MNIST: 1, Cifar10/Cifar100: 3, parents: 3, parents_img: 6")
DEFINE_integer("img_size", 32, "enlarge image size")
DEFINE_integer("n_aug_img",1 , "if 2: num_img: 55000 -> aug_img: 110000, elif 1: False")
DEFINE_string("data_source","cifar10","either 'mnist', 'cifar10', 'fashion_mnist' or anything else to load a custom dataset ")
```

The "data_source" needs to be set to the dataset you want to use. In case of Cifar10, MNIST or FashionMNIST, data will be loaded from the Keras datasets directly. If you want to use custom data, you will need to put it as images in the train, validation and test data directories above, with a subdirectory for each class.
Note that channels need to be set at same value as the type of image (3 for RGB images, 1 for greyscale images).

### Execution

To run the controller and find architectures you need to use the following code:

```
python main_controller_child_trainer.py

```

The best architectures found will be in the best_arcs_filename.
To train one, you need to run the following code (the sequence is an example for Cifar10):

```
python main_child_trainer.py --child_fixed_arc "1 2 1 3 0 1 0 4 1 1 1 1 0 1 0 1 1 0 0 1 0 1 0 4 1 0 2 0 0 3 1 1 0 0 0 0 4 1 1 0"

```

## Datasets

In our code we allow quick set-up for 3 datasets, which are included in the Keras datasets module for TensorFlow.
We also used these datasets to compare I-ENAS with ENAS.

[MNIST](https://keras.io/api/datasets/mnist/)

[FashionMNIST](https://keras.io/api/datasets/fashion_mnist/)

[Cifar10](https://keras.io/api/datasets/cifar10/)

## Results

Each of the plots shown below contains the accuracies of the child architectures generated by the controller for both I-ENAS and ENAS, plus a moving average for each of them for ease of comparison.  
The highlighted data points show where both methods first achieved their maximum accuracy. 

ENAS computes the accuracy on a random minibatch of 128 data, so that needs to be kept into account when interpreting the results. The 100% accuracy reached in MNIST is only a symptom of this calculation mixing with a very simple classification task.

### MNIST

In MNIST both methods performed very well due to the simplicity of the task, however I-ENAS still showed a remarkable ability to reach its maximum faster than ENAS.

![MNIST I-ENAS vs ENAS](https://gitlab.com/g.magistrali/improved-enas/-/raw/dev-accuracy_scaling/images/ienas_mnist.png "MNIST")

### FashionMNIST

In FashionMNIST the performance was far closer. As we show later in a comparison of average accuracy and standard deviation, I-ENAS still provided measurably better results.

![FashionMNIST I-ENAS vs ENAS](https://gitlab.com/g.magistrali/improved-enas/-/raw/dev-accuracy_scaling/images/ienas_fashionmnist.png "FashionMNIST")

### Cifar10

Cifar10 proved a harder task for both methods, but I-ENAS managed to reach a significantly higher accuracy and generate networks more reliably accurate.

![Cifar10 I-ENAS vs ENAS](https://gitlab.com/g.magistrali/improved-enas/-/raw/dev-accuracy_scaling/images/ienas_cifar10.png "Cifar10")

This is the reduction cell found by I-ENAS when it reached its highest accuracy:

![Cifar 10 reduction cell](https://gitlab.com/g.magistrali/improved-enas/-/raw/dev-accuracy_scaling/images/ienas_cifar10_rcell.png "Cifar10 Reduction Cell")

### Comparison

To show the difference in performance between the two methods, we calculated the average accuracy and the standard deviation during the epoch where I-ENAS first reached its maximum value:

![I-ENAS vs ENAS Comparison Table](https://gitlab.com/g.magistrali/improved-enas/-/raw/dev-accuracy_scaling/images/ienas_enas_comparison.png "I-ENAS vs ENAS Comparison Table")

As we can see, in all of these situations I-ENAS generated better networks in a more reliable way.

## Citation
```bibtex
@INPROCEEDINGS{Gallo:2020:IVCNZ, 
  author={Ignazio Gallo, Gabriele Magistrali, Nicola Landro and Riccardo La Grassa},
  booktitle={International Conference on Image and Vision Computing New Zealand (IVCNZ 2020)}, 
  title={Improving the Efficient Neural Architecture Searchvia Rewarding Modifications},
  year={2020}, 
  month={Sept},
}

```

